package uk.ac.gcu.katrinhartmann.messageboard;

import java.util.Date;

public class Message {
    private String subject;
    private String contents;
    private Date sentDate;

    public Message(String s, String c, Date d) {
        subject = s;
        contents = c;
        sentDate = d;
    }

    public String displayMessage() {
        StringBuilder b = new StringBuilder();
        b.append("Subject: "); b.append(subject); b.append('\n');
        b.append(sentDate); b.append('\n');
        b.append(contents); b.append('\n');
        return b.toString();
    }
}
